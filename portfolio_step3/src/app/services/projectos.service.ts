import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})

export class ProjectosService {
    projectos = [
        {
            id: '0',
            nome: 'Facebook',
            url: 'https://www.facebook.com',
            imagem: 'assets/facebook.jpg',
            detalhe: 'Donec id tortor sit amet erat molestie sollicitudin. Vestibulum aliquet dolor nisi, sit amet auctor nulla imperdiet imperdiet. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Praesent feugiat magna velit, at volutpat metus placerat vel. Vivamus vitae fermentum massa.'
        },
        {
            id: '1',
            nome: 'Twitter',
            url: 'https://www.twitter.com',
            imagem: 'assets/twitter.jpg',
            detalhe: 'Ut sit amet finibus lorem, viverra dapibus nunc. Proin aliquet dolor at ante sagittis porta. Maecenas sit amet lacus enim. Quisque id finibus nunc. Aliquam lorem ante, ullamcorper non sodales vitae, fermentum a tortor. Cras eu enim sit amet mi rutrum pulvinar sed eu sapien. Nunc est lacus, congue ullamcorper eros eget, luctus dapibus orci.'
        },
        {
            id: '2',
            nome: 'Instagram',
            url: 'https://www.instagram.com',
            imagem: 'assets/instagram.jpg',
            detalhe: 'Aenean ut ullamcorper quam. Morbi egestas porta aliquet. Nunc nibh risus, dapibus nec feugiat quis, commodo quis augue. Pellentesque mattis velit a augue blandit volutpat. Fusce vel eleifend purus.'
        },
        {
            id: '3',
            nome: 'bitbucket',
            url: 'https://www.bitbucket.com',
            imagem: 'assets/bitbucket.png',
            detalhe: 'Morbi mattis orci vitae diam commodo, eget aliquam velit tincidunt. Ut non pellentesque augue, interdum finibus urna. Morbi luctus placerat arcu. In dolor nunc, aliquam at blandit et, elementum et lorem. Morbi mattis hendrerit velit, nec auctor quam eleifend nec.'
        },
        {
            id: '4',
            nome: 'Stack Overflow',
            url: 'https://www.stackoverflow.com',
            imagem: 'assets/stackoverflow.png',
            detalhe: 'Sed elementum augue ut hendrerit elementum. Nunc congue neque in quam ultricies, non sagittis mauris consequat. Donec pretium augue et urna porttitor, id semper mauris lacinia. Phasellus egestas nibh urna, a finibus turpis tincidunt pharetra. '
        },
    ];

    obter() {
        return this.projectos;
    }

    obterProjectoPorId(id) {
        console.log(id)
        return this.projectos.find(projecto => projecto.id == id);
    }
}
