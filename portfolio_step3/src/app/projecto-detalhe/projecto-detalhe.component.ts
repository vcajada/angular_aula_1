import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ProjectosService } from '../services/projectos.service';

@Component({
    selector: 'app-projecto-detalhe',
    templateUrl: './projecto-detalhe.component.html',
    styleUrls: ['./projecto-detalhe.component.css']
})

export class ProjectoDetalheComponent implements OnInit {

    constructor(private route: ActivatedRoute, private projectosService: ProjectosService) {}

    projecto = {};

    ngOnInit() {
        this.projecto = this.projectosService.obterProjectoPorId(this.route.snapshot.params.id);
        console.log(this.projecto);
    }
}
