import { Component, Input, OnInit } from '@angular/core';
import {
    trigger,
    state,
    style,
    animate,
    transition
} from '@angular/animations';

@Component({
    selector: 'app-projecto',
    templateUrl: './projecto.component.html',
    styleUrls: ['./projecto.component.css'],
    animations: [
        trigger('deslizarEsquerdo', [
            state('visivel', style({
                left: 0,
                opacity: 1
            })),
            state('escondido', style({
                left: '-100%',
                opacity: 0
            })),
            transition('escondido => visivel', [
                animate('0.3s')
            ])
        ]),
        trigger('deslizarDireito', [
            state('visivel', style({
                right: 0,
                opacity: 1
            })),
            state('escondido', style({
                right: '-100%',
                opacity: 0
            })),
            transition('escondido => visivel', [
                animate('0.3s')
            ])
        ])
    ]
})

export class ProjectoComponent implements OnInit {

    @Input() projecto;

    ngOnInit() {
        window.addEventListener('scroll', this.scroll, true);
    }

    scroll(e) {
        console.log(e);
    }
}
