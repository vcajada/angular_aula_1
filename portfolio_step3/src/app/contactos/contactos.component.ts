import { Component } from '@angular/core';
import { ContactosService } from '../services/contactos.service';
import {
    trigger,
    state,
    style,
    animate,
    transition
} from '@angular/animations';

@Component({
    selector: 'app-contactos',
    templateUrl: './contactos.component.html',
    styleUrls: ['./contactos.component.css'],
    animations: [
        trigger('abrirFechar', [
            state('aberto', style({
                bottom: '0',
            })),
            state('fechado', style({
                bottom: '-100%'
            })),
            transition('aberto => fechado', [
                animate('0.3s')
            ]),
            transition('fechado => aberto', [
                animate('0.3s')
            ]),
        ]),
    ]
})

export class ContactosComponent {

    constructor(private contactosService: ContactosService) {}

    fechar() {
        this.contactosService.alterarAberto(false);
    }
    abrir() {
        this.contactosService.alterarAberto(true);
    }

    estaAberto() {
        return this.contactosService.obterAberto();
    }
}
