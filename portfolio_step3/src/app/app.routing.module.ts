import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { InicioComponent } from './inicio/inicio.component';
import { SobreComponent } from './sobre/sobre.component';
import { ProjectosComponent } from './projectos/projectos.component';
import { ProjectoDetalheComponent } from './projecto-detalhe/projecto-detalhe.component';

const routes = [
    {
        path: 'inicio',
        component: InicioComponent
    },
    {
        path: 'projectos',
        component: ProjectosComponent
    },
    {
        path: 'sobre',
        component: SobreComponent
    },
    {
        path: 'projecto/:id',
        component: ProjectoDetalheComponent
    },
    {
        path: '**',
        component: InicioComponent
    },
];

@NgModule({
    imports: [ RouterModule.forRoot(routes)],
    exports: [ RouterModule ]
})

export class AppRoutingModule {}
