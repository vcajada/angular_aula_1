const  produtos = [
    {
        nome: 'banana',
        tipo: 'fruta',
        preco: '1.50',
        estado: false,
        selecionado: false
    },
    {
        nome: 'papel',
        tipo: 'escritorio',
        preco: '2.25',
        estado: false,
        selecionado: false
    },
    {
        nome: 'borracha',
        tipo: 'escritorio',
        preco: '5.5',
        estado: false,
        selecionado: false
    }
];

export default produtos;
