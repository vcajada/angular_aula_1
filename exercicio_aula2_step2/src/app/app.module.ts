import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { ProdutoComponent } from './produto/produto.component';
import { ListaComprasComponent } from './lista-compras/lista-compras.component';
import { AdicionarProdutoComponent } from './adicionar-produto/adicionar-produto.component';
import { HeaderComponent } from './header/header.component';
import { ProdutoDetalheComponent } from './produto-detalhe/produto-detalhe.component';

@NgModule({
    declarations: [
        AppComponent,
        ProdutoComponent,
        ListaComprasComponent,
        AdicionarProdutoComponent,
        HeaderComponent,
        ProdutoDetalheComponent
    ],
    imports: [
        BrowserModule,
        FormsModule
    ],
    providers: [],
    bootstrap: [AppComponent]
})

export class AppModule {}
