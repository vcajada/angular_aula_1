import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
    selector: 'app-produto',
    templateUrl: './produto.component.html',
    styleUrls: ['./produto.component.css']
})
export class ProdutoComponent {

    @Input() produto;
    @Input() index;
    @Output() remover = new EventEmitter();
    @Output() marcar = new EventEmitter();
    @Output() selecionar = new EventEmitter();

    emitirRemover() {
        this.remover.emit(this.index);
    }

    emitirAlternarSelecionar(evento) {
        evento.stopPropagation();
        this.selecionar.emit(this.index);
    }

    emitirAlternarMarcarFeito() {
        this.marcar.emit(this.index);
    }

    botaoDetalheValor() {
        if (this.produto.selecionado) {
            return '-';
        } else {
            return '+';
        }
    }

}
