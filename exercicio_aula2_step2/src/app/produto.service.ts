import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})

export default class ProdutoService {

    produtos = [
        {
            nome: 'banana',
            tipo: 'fruta',
            preco: '1.50',
            estado: false,
            selecionado: false
        },
        {
            nome: 'papel',
            tipo: 'escritorio',
            preco: '2.25',
            estado: false,
            selecionado: false
        },
        {
            nome: 'borracha',
            tipo: 'escritorio',
            preco: '5.5',
            estado: false,
            selecionado: false
        }
    ];

    remover(index) {
        this.produtos.splice(index, 1);
    }

    alterar(index, produto) {
        this.produtos[index] = produto;
    }

    adicionar(produto) {
        this.produtos.push(produto);
    }
}
