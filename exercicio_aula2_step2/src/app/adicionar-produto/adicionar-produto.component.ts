import { Component, Output, EventEmitter } from '@angular/core';

@Component({
    selector: 'app-adicionar-produto',
    templateUrl: './adicionar-produto.component.html',
    styleUrls: ['./adicionar-produto.component.css']
})
export class AdicionarProdutoComponent {

    @Output() adicionarProduto = new EventEmitter();

    tituloAdicionar = 'Adicionar Produto';

    produto = {
        nome: '',
        tipo: '',
        preco: '',
        estado: false,
        selecionado: false
    };

    emitirAdicionarProduto() {
        this.adicionarProduto.emit({...this.produto});
        this.produto.nome = '';
        this.produto.tipo = '';
        this.produto.preco = '';
    }
}
