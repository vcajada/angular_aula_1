import { Component } from '@angular/core';
import ProdutoService from '../produto.service';

@Component({
    selector: 'app-lista-compras',
    templateUrl: './lista-compras.component.html',
    styleUrls: ['./lista-compras.component.css']
})
export class ListaComprasComponent {

    constructor(private produtoService: ProdutoService) {}

    tituloLista = 'Lista de Tarefas';

    remover(index) {
        this.produtoService.remover(index);
    }

    alternarMarcar(index) {
        const produtos = this.getProdutos();
        const produto = produtos[index];
        produto.estado = !produto.estado;

        this.produtoService.update(produto);
    }

    alternarSelecionar(index) {
        const produtos = this.getProdutos();
        const produto = produtos[index];

        produtos.forEach((item, j) => {
            if (j === index) { return; }
            item.selecionado = false;
        });

        produto.selecionado = !produto.selecionado;
        this.produtoService.update(produto);
    }

    getProdutos() {
        return this.produtoService.produtos;
    }
}
