import { Component, Input, OnInit } from '@angular/core';

@Component({
    selector: 'app-produto',
    templateUrl: './produto.component.html',
    styleUrls: ['./produto.component.css']
})
export class ProdutoComponent implements OnInit {

    constructor() { }

    @Input() produto;

    adicionarCarrinho() {
    }

    ngOnInit() {
    }

}
