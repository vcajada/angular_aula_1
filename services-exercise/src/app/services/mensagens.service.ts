import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})

export class MensagensService {
    mensagem = '';
    timeoutId;

    mudarMensagem(mensagem) {
        this.mensagem = mensagem;

        clearTimeout(this.timeoutId);
        this.timeoutId = setTimeout(() => this.mensagem = '', 7000);
    }
}
