import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class ProdutosService {
    produtos = [];
    produtosCarrinho = [];

    carregarProdutos() {
        fetch('https://my-json-server.typicode.com/vascocajada/angular_etic_db/produtos')
            .then(res => res.json())
            .then(json => {
                this.produtos = json;
            }
        );
    }

    adicionarProdutoCarrinho(produto) {
        this.produtosCarrinho.push(produto);
    }

    removerProdutoCarrinho(index) {
        this.produtosCarrinho.splice(index, 1);
    }
}
