import { Component, Input, OnInit } from '@angular/core';
import { ProdutosService } from '../services/produtos.service';
import { MensagensService } from '../services/mensagens.service';

@Component({
    selector: 'app-produto',
    templateUrl: './produto.component.html',
    styleUrls: ['./produto.component.css']
})
export class ProdutoComponent implements OnInit {

    constructor(private produtosService: ProdutosService, private mensagensService: MensagensService) { }

    @Input() produto;

    adicionarCarrinho() {
        this.produtosService.adicionarProdutoCarrinho(this.produto);
        this.mensagensService.mudarMensagem(`${this.produto.title} adicionado ao carrinho com sucesso`);
    }

    ngOnInit() {
    }

}
