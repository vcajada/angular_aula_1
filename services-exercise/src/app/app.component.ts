import { Component, OnInit } from '@angular/core';
import { PaginasService } from './services/paginas.service';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit {

    constructor(private paginasService: PaginasService) {}

    obterPagina() {
        return this.paginasService.pagina;
    }

    ngOnInit() {
    }
}
