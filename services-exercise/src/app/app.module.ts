import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { GrelhaComponent } from './grelha/grelha.component';
import { ProdutoComponent } from './produto/produto.component';
import { NavbarComponent } from './navbar/navbar.component';
import { CarrinhoComponent } from './carrinho/carrinho.component';
import { MensagemComponent } from './mensagem/mensagem.component';

@NgModule({
    declarations: [
        AppComponent,
        GrelhaComponent,
        ProdutoComponent,
        NavbarComponent,
        CarrinhoComponent,
        MensagemComponent
    ],
    imports: [
        BrowserModule
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule { }
