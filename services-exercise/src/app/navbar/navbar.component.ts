import { Component, OnInit } from '@angular/core';
import { PaginasService } from '../services/paginas.service';

@Component({
    selector: 'app-navbar',
    templateUrl: './navbar.component.html',
    styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

    constructor(private paginasService: PaginasService) { }

    mudarPagina(valor) {
        this.paginasService.pagina = valor;
    }

    ngOnInit() {
    }
}
