import { Component, OnInit } from '@angular/core';
import { ProdutosService } from '../services/produtos.service';
import { MensagensService } from '../services/mensagens.service';

@Component({
    selector: 'app-carrinho',
    templateUrl: './carrinho.component.html',
    styleUrls: ['./carrinho.component.css']
})
export class CarrinhoComponent implements OnInit {

    constructor(private produtosService: ProdutosService, private mensagensService: MensagensService) { }

    obterProdutos() {
        return this.produtosService.produtosCarrinho;
    }

    removerProdutoCarrinho(index) {
        this.mensagensService.mudarMensagem(`${this.obterProdutos()[index].title} removido do carrinho com sucesso`);
        this.produtosService.removerProdutoCarrinho(index);
    }

    ngOnInit() {
    }
}
