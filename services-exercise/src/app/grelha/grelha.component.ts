import { Component, OnInit } from '@angular/core';
import { ProdutosService } from '../services/produtos.service';

@Component({
    selector: 'app-grelha',
    templateUrl: './grelha.component.html',
    styleUrls: ['./grelha.component.css']
})

export class GrelhaComponent implements OnInit {

    constructor(private produtosService: ProdutosService) {}

    ngOnInit() {
        this.produtosService.carregarProdutos();
    }

    obterProdutos() {
        return this.produtosService.produtos;
    }
}
