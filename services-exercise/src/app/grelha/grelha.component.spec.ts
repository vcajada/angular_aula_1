import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GrelhaComponent } from './grelha.component';

describe('GrelhaComponent', () => {
  let component: GrelhaComponent;
  let fixture: ComponentFixture<GrelhaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GrelhaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GrelhaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
