import { Component, OnInit } from '@angular/core';
import { MensagensService } from '../services/mensagens.service';

@Component({
    selector: 'app-mensagem',
    templateUrl: './mensagem.component.html',
    styleUrls: ['./mensagem.component.css']
})
export class MensagemComponent implements OnInit {

    constructor(private mensagensService: MensagensService) { }

    obterMensagem() {
        return this.mensagensService.mensagem;
    }

    ngOnInit() {
    }

}
