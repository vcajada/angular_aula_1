import { Component } from '@angular/core';
import { ContactosService } from '../services/contactos.service';

@Component({
    selector: 'app-menu',
    templateUrl: './menu.component.html',
    styleUrls: ['./menu.component.css']
})

export class MenuComponent {

    constructor(private contactosService: ContactosService) {}

    alternarContactosAberto() {
        this.contactosService.alterarAberto(!this.contactosService.obterAberto());
    }

    estaAberto() {
        this.contactosService.obterAberto();
    }
}
