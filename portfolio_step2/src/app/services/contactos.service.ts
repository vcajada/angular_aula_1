import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})

export class ContactosService {
    estaAberto = false;

    alterarAberto(estaAberto) {
        this.estaAberto = estaAberto;
    }

    obterAberto() {
        return this.estaAberto;
    }
}
