import { Component } from '@angular/core';

@Component({
    selector: 'app-sobre',
    templateUrl: './sobre.component.html',
    styleUrls: ['./sobre.component.css' ]
})

export class SobreComponent {
    titulo = 'Sobre';
    profile = {
        nome: 'Nome',
        apelido: 'Apelido',
        cidade: 'Lisboa',
        dataNascimento: '1986-05-01'
    };

    idade() {
        const dataNascimento = new Date(this.profile.dataNascimento);
        const diff = Date.now() - dataNascimento.getTime();
        const idade = new Date(diff);
        return Math.abs(idade.getUTCFullYear() - 1970);

    }
}
