import { Component } from '@angular/core';
import { ProjectosService } from '../services/projectos.service';

@Component({
    selector: 'app-projectos',
    templateUrl: './projectos.component.html',
    styleUrls: ['./projectos.component.css']
})

export class ProjectosComponent {

    constructor(private projectosService: ProjectosService) {}

    titulo = 'Projectos';

    getProjectos() {
        return this.projectosService.obter();
    }
}
