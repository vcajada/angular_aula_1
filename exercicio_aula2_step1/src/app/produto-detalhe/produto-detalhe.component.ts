import { Component, Input } from '@angular/core';

@Component({
    selector: 'app-produto-detalhe',
    templateUrl: './produto-detalhe.component.html',
    styleUrls: ['./produto-detalhe.component.css']
})

export class ProdutoDetalheComponent {

    @Input() produto;

    estado() {
        if (this.produto.estado) {
           return 'feito';
        } else {
            return 'por fazer';
        }
    }
}
