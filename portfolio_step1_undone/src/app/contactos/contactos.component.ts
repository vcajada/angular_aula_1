import { Component } from '@angular/core';
import { ContactosService } from '../services/contactos.service';

@Component({
    selector: 'app-contactos',
    templateUrl: './contactos.component.html',
    styleUrls: ['./contactos.component.css'],
})

export class ContactosComponent {

    constructor(private contactosService: ContactosService) {}

    fechar() {
    }

    abrir() {
    }

    estaAberto() {
    }
}
