import { Component } from '@angular/core';

@Component({
    selector: 'app-projectos',
    templateUrl: './projectos.component.html',
    styleUrls: ['./projectos.component.css']
})

export class ProjectosComponent {

    titulo = 'Projectos';
}
