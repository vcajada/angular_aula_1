import { Component } from '@angular/core';

@Component({
    selector: 'app-sobre',
    templateUrl: './sobre.component.html',
    styleUrls: ['./sobre.component.css' ]
})

export class SobreComponent {
    titulo = 'Sobre';
    profile = {
        nome: 'Nome',
        apelido: 'Apelido',
        cidade: 'Lisboa',
        dataNascimento: '1986-05-01'
    };
}
