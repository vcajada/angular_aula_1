import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { MenuComponent } from './menu/menu.component';
import { LogoComponent } from './logo/logo.component';
import { InicioComponent } from './inicio/inicio.component';
import { ProjectosComponent } from './projectos/projectos.component';
import { SobreComponent } from './sobre/sobre.component';

@NgModule({
    declarations: [
        AppComponent,
        MenuComponent,
        LogoComponent,
        InicioComponent,
        ProjectosComponent,
        SobreComponent
    ],
    imports: [
        BrowserModule
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule { }
