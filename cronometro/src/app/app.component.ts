import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
    minutos = 0;
    segundos = 0;
    interval;
    temporizadorAcabou = false;
    tipo = '';

    contador() {
        return `${this.minutos}:${this.segundos}`;
    }

    comecar(evento) {
        if (this.interval && this.tipo === evento.tipo) { return; }

        this.parar();
        this.tipo = evento.tipo;

        if (evento.tipo === 'cronometro') {
            this.comecarCronometro();
        } else if (evento.tipo === 'temporizador') {
            this.comecarTemporizador(evento);
        }
    }

    comecarTemporizador(evento) {
        this.minutos = evento.minuto;
        this.segundos = evento.segundo;

        this.interval = setInterval(() => {
            if (this.segundos === 0 && this.minutos === 0) {
                this.temporizadorAcabou = true;
                this.parar();
            } else if (this.segundos === 0) {
                this.segundos = 59;
                this.minutos--;
            } else {
                this.segundos--;
            }
        }, 1000);
    }

    comecarCronometro() {

        this.interval = setInterval(() => {
            if (this.segundos > 58) {
                this.minutos++;
                this.segundos = 0;
            } else {
                this.segundos++;
            }
        }, 1000);
    }

    parar() {
        clearInterval(this.interval);
        this.interval = 0;
        this.tipo = '';
    }

    reset() {
        this.parar();
        this.minutos = 0;
        this.segundos = 0;
    }
}
