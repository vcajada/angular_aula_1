import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfigurarCronometroComponent } from './configurar-cronometro.component';

describe('ConfigurarCronometroComponent', () => {
  let component: ConfigurarCronometroComponent;
  let fixture: ComponentFixture<ConfigurarCronometroComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfigurarCronometroComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfigurarCronometroComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
