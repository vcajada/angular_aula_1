import { Component, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-configurar-cronometro',
  templateUrl: './configurar-cronometro.component.html',
  styleUrls: ['./configurar-cronometro.component.css']
})
export class ConfigurarCronometroComponent {

  constructor() { }

  @Output() eventoComecar = new EventEmitter();
  @Output() eventoParar = new EventEmitter();
  @Output() eventoReset = new EventEmitter();

  emitirComecar() {
    this.eventoComecar.emit({ tipo: 'cronometro' });
  }

  emitirParar() {
    this.eventoParar.emit();
  }

  emitirReset() {
    this.eventoReset.emit();
  }

}
