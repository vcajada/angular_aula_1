import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-configurar',
  templateUrl: './configurar.component.html',
  styleUrls: ['./configurar.component.css']
})
export class ConfigurarComponent implements OnInit {

  constructor() { }

  @Output() eventoComecar = new EventEmitter();
  @Output() eventoParar = new EventEmitter();
  @Output() eventoReset = new EventEmitter();

  configurar = 'cronometro';

  emitirComecar(evento) {
    this.eventoComecar.emit(evento);
  }

  emitirParar() {
    this.eventoParar.emit();
  }

  emitirReset() {
    this.eventoReset.emit();
  }

  ngOnInit() {
  }

}
