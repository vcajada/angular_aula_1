import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-configurar-temporizador',
  templateUrl: './configurar-temporizador.component.html',
  styleUrls: ['./configurar-temporizador.component.css']
})
export class ConfigurarTemporizadorComponent implements OnInit {

  constructor() { }

  minutos = Array(24).fill(0).map((x, i) => i);
  segundos = Array(59).fill(0).map((x, i) => i);

  minuto = 0;
  segundo = 0;

  @Output() eventoComecar = new EventEmitter();
  @Output() eventoParar = new EventEmitter();
  @Output() eventoReset = new EventEmitter();

  emitirComecar() {
    this.eventoComecar.emit({ tipo: 'temporizador', minuto: this.minuto, segundo: this.segundo });
  }

  emitirParar() {
    this.eventoParar.emit();
  }

  emitirReset() {
    this.eventoReset.emit();
  }

  ngOnInit() {
  }

}
