import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfigurarTemporizadorComponent } from './configurar-temporizador.component';

describe('ConfigurarTemporizadorComponent', () => {
  let component: ConfigurarTemporizadorComponent;
  let fixture: ComponentFixture<ConfigurarTemporizadorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfigurarTemporizadorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfigurarTemporizadorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
