import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-montra',
  templateUrl: './montra.component.html',
  styleUrls: ['./montra.component.css']
})
export class MontraComponent implements OnInit {

  constructor() { }

  @Input() contador;

  ngOnInit() {
  }

}
