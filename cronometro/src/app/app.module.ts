import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { MontraComponent } from './montra/montra.component';
import { ConfigurarComponent } from './configurar/configurar.component';
import { ConfigurarCronometroComponent } from './configurar-cronometro/configurar-cronometro.component';
import { ConfigurarTemporizadorComponent } from './configurar-temporizador/configurar-temporizador.component';

@NgModule({
  declarations: [
    AppComponent,
    MontraComponent,
    ConfigurarComponent,
    ConfigurarCronometroComponent,
    ConfigurarTemporizadorComponent
  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
