import { Component, Input } from '@angular/core';

@Component({
    selector: 'app-lista-compras',
    templateUrl: './lista-compras.component.html',
    styleUrls: ['./lista-compras.component.css']
})
export class ListaComprasComponent {

    @Input() produtos;

    tituloLista = 'Lista de Tarefas';

    remover(index) {
        this.produtos.splice(index, 1);
    }

    alternarMarcar(index) {
        // TODO
    }

    alternarSelecionar(index) {
        // TODO
        this.produtos[index].selecionado = !this.produtos[index].selecionado;
    }
}
