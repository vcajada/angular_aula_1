import { Component } from '@angular/core';

@Component({
    selector: 'app-adicionar-produto',
    templateUrl: './adicionar-produto.component.html',
    styleUrls: ['./adicionar-produto.component.css']
})
export class AdicionarProdutoComponent {

    tituloAdicionar = 'Adicionar Produto';

    produto = {
        nome: '',
        estado: false,
        tipo: '',
        selecionado: false
    };

    emitirAdicionarProduto() {
        // TODO
    }
}
