import { Component } from '@angular/core';
import produtos from '../assets/mock_data';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})

export class AppComponent {

    produtos = produtos;

    adicionarProduto(produto) {
        this.produtos.push(produto);
    }
}
