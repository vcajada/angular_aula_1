import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { MontraComponent } from './montra/montra.component';
import { ConfigurarComponent } from './configurar/configurar.component';

@NgModule({
  declarations: [
    AppComponent,
    MontraComponent,
    ConfigurarComponent,
  ],
  imports: [
    BrowserModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
