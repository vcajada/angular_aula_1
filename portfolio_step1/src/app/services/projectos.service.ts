import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})

export class ProjectosService {
    projectos = [
        {
            nome: 'Facebook',
            url: 'https://www.facebook.com',
            imagem: 'assets/facebook.jpg'
        },
        {
            nome: 'Twitter',
            url: 'https://www.twitter.com',
            imagem: 'assets/twitter.jpg'
        },
        {
            nome: 'Instagram',
            url: 'https://www.instagram.com',
            imagem: 'assets/instagram.jpg'
        },
        {
            nome: 'bitbucket',
            url: 'https://www.bitbucket.com',
            imagem: 'assets/bitbucket.png'
        },
        {
            nome: 'Stack Overflow',
            url: 'https://www.stackoverflow.com',
            imagem: 'assets/stackoverflow.png'
        },
    ];

    get() {
        return this.projectos;
    }
}
