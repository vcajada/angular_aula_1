import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app.routing.module';

import { AppComponent } from './app.component';
import { MenuComponent } from './menu/menu.component';
import { LogoComponent } from './logo/logo.component';
import { TituloComponent } from './titulo/titulo.component';
import { InicioComponent } from './inicio/inicio.component';
import { ProjectosComponent } from './projectos/projectos.component';
import { ProjectoComponent } from './projecto/projecto.component';
import { SobreComponent } from './sobre/sobre.component';

@NgModule({
    declarations: [
        AppComponent,
        MenuComponent,
        LogoComponent,
        TituloComponent,
        InicioComponent,
        ProjectosComponent,
        ProjectoComponent,
        SobreComponent
    ],
    imports: [
        BrowserModule,
        AppRoutingModule
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule { }
